import java.io.InputStreamReader;
import java.util.Scanner;

public class maxsubslow{
public static void main(String[] args) {
        Scanner input = new Scanner(new InputStreamReader(System.in));
        int num = input.nextInt();
        int[] A = new int[num];

        for(int i=0; i<num; i++){
            A[i] = input.nextInt();
        }

        System.out.println(maxSubSlow(A));


    }
    public static int maxSubSlow(int[] A) {
        int max = 0;
        for(int j=0; j<A.length; j++){
            for (int k=j; k<A.length; k++){
                int sum =0;
                for (int i=j; i<=k; i++){
                    sum = sum + A[i];
                }
                if (sum > max ){
                    max = sum;
                }
            }
        }
        return max;
    }
}